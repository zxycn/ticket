package com.qianxunclub.ticket.ticket;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.qianxunclub.ticket.constant.StatusEnum;
import com.qianxunclub.ticket.model.BuyTicketInfoModel;
import com.qianxunclub.ticket.model.UserTicketStore;
import com.qianxunclub.ticket.repository.dao.TicketDao;
import com.qianxunclub.ticket.repository.entity.Ticket;
import com.qianxunclub.ticket.util.CommonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.concurrent.*;

import lombok.extern.slf4j.Slf4j;

/**
 * @author zhangbin
 * @date 2019-06-08 11:54
 */
@Slf4j
@Component
public class DoHandle {

    @Autowired
    private Login login;
    @Autowired
    private TicketDao ticketDao;

    private final static LocalTime startTime = LocalTime.parse("05:55");
    private final static LocalTime endTime = LocalTime.parse("23:35");

    /**
     * 定义线程池
     * java四种线程池简介，使用 https://www.cnblogs.com/zhangyaxiao/p/8268549.html
     * https://www.liaoxuefeng.com/wiki/1252599548343744/1306581130018849
     */
    private static ExecutorService handleCachedThreadPool = Executors.newFixedThreadPool(100);

    /**
     * 定义线程池2（自定义线程名称）
     * https://blog.csdn.net/qq_27409289/article/details/78041206
     * https://www.cnblogs.com/wanghongsen/p/12332350.html
     */
    private static ThreadPoolExecutor executorPool = new ThreadPoolExecutor(
            5,
            100,
            60L,
            TimeUnit.SECONDS,
            new ArrayBlockingQueue<Runnable>(1000),
            new ThreadFactoryBuilder().setNameFormat("demo-pool-%d").build(),
            new ThreadPoolExecutor.DiscardOldestPolicy()//对拒绝任务不抛弃，而是抛弃队列里面等待最久的一个线程，然后把拒绝任务加到队列。
    );

    public void go() {
        UserTicketStore.buyTicketInfoModelList.forEach(buyTicketInfoModel -> {
            // 开启抢票线程
            this.add(buyTicketInfoModel);
        });
    }

    public void add(BuyTicketInfoModel buyTicketInfoModel) {
        // 模拟登录失败
        if (!login.login(buyTicketInfoModel)) {
            UserTicketStore.httpUtilStore.remove(buyTicketInfoModel.getUsername());
            return;
        }

        // 设置主线程名称
        Thread.currentThread().setName(CommonUtil.getThreadName(buyTicketInfoModel));

        // 启动主线程
        Main main = new Main(buyTicketInfoModel);
        Thread thread = new Thread(main);
        thread.start();
    }

    /**
     * 抢票主线程
     */
    class Main implements Runnable {
        // 成员变量（数据实体）
        private BuyTicketInfoModel buyTicketInfoModel;

        public Main(BuyTicketInfoModel buyTicketInfoModel) {
            this.buyTicketInfoModel = buyTicketInfoModel;
        }

        @Override
        public void run() {
            // 设置抢票线程名称
            Thread.currentThread().setName(CommonUtil.getThreadName(buyTicketInfoModel));
            // 抢票状态
            buyTicketInfoModel.setStatus(StatusEnum.ING);
            while (true) {
                try {
                    // 检查时段
                    if (!checkWorkTime()) {
                        continue;
                    }
                    // 定义抢票任务
                    Task task = new Task(buyTicketInfoModel);
                    // 向线程池提交任务
                    Future<Boolean> booleanFuture = handleCachedThreadPool.submit(task);
                    // 取任务在线程池中线程执行的结果
                    Boolean flag = booleanFuture.get();
                    if (flag) {
                        log.info("完成!!!!");
                        // 设置状态
                        buyTicketInfoModel.setStatus(StatusEnum.SUCCESS);
                        Ticket ticket = ticketDao.getTicketByUserName(buyTicketInfoModel.getUsername());
                        // 有票
                        if (ticket != null) {
                            // 删除这个抢票任务
                            ticketDao.deleteById(buyTicketInfoModel.getId());
                        }
                        // 结束此次抢票任务
                        return;
                    }
                } catch (Exception e) {
                    log.error("未知异常", e);
                } finally {
                    try {
                        // 停1s 再来
                        TimeUnit.MILLISECONDS.sleep(1000);
                    } catch (InterruptedException ex) {
                        log.error("time sleep error", ex);
                    }
                }
            }
        }

    }

    private boolean checkWorkTime() throws InterruptedException {
        if (LocalTime.now().isAfter(endTime) || LocalTime.now().isBefore(startTime)) {
            log.info("不在抢票范围内，运营时间为:06:00-23:30");
            TimeUnit.MINUTES.sleep(1);
            return false;
        }
        return true;
    }
}
