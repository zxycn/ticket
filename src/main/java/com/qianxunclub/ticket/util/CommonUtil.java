package com.qianxunclub.ticket.util;

import com.qianxunclub.ticket.model.BuyTicketInfoModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zhangbin
 * @date 2019-06-06 17:36
 */
public class CommonUtil {

    /**
     * 组装线程名称
     * 日志中会用到
     */
    public static String getThreadName(BuyTicketInfoModel buyTicketInfoModel) {
        return "" +
                "👤" + buyTicketInfoModel.getUsername() +
                "[" + buyTicketInfoModel.getRealName() + "-" + buyTicketInfoModel.getTrainNumber() + "]" +
                "-" + buyTicketInfoModel.getMobile();
    }

    /**
     * 提取字符串中指定的字符
     *
     * @param regex 定义的规则
     * @param str   需要截取的字符串
     * @return 截取出来的字符串
     */
    public static String regString(String regex, String str) {
        Pattern pa = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher ma = pa.matcher(str);
        //往下遍历
        if (ma.find()) {
            return ma.group();
        }
        return "";
    }

    /**
     * 转 date 为 GMT 格式
     *
     * @param date
     * @return
     */
    public static String getGMT(String date) {
        String str = "";
        TimeZone tz = TimeZone.getTimeZone("ETC/GMT-8");
        TimeZone.setDefault(tz);
        Calendar cal = Calendar.getInstance(new SimpleTimeZone(0, "GMT"));
        Date dd;
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss 'GMT'", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            dd = shortSdf.parse(date);
            cal.setTime(dd);
            str = sdf.format(cal.getTime());
            return str + "+0800 (中国标准时间)";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
