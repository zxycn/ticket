package com.qianxunclub.ticket.model;

import com.qianxunclub.ticket.util.HttpUtil;
import org.apache.http.impl.client.BasicCookieStore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 线程中保存的正在抢票的信息
 *
 * @author zhangbin
 * @date 2019-06-08 11:10
 */
public class UserTicketStore {

    public static List<BuyTicketInfoModel> buyTicketInfoModelList = new ArrayList<>();
    public static Map<String, HttpUtil> httpUtilStore = new HashMap<>();

    public static void add(BuyTicketInfoModel buyTicketInfoModel) {
        buyTicketInfoModelList.add(buyTicketInfoModel);
    }
}
